<?php
		
class admin_lienController extends ActionController {
	public function firstAction() {
		$this->view->_layout('admin');
		View::appendStyle (Url::display ('/themes/default/admin.css'));

		if (!is_admin ()) {
			MinzError::error (
				403,
				array ('warning' => array ('Vous n\'avez pas le droit d\'accéder à cette page'))
			);
		}
	}

	public function indexAction() {
		$lienDAO = new LienDAO();
		$this->view->liens = $lienDAO->lister();
		
		$this->view->appendTitle('Gestion des liens - ');
	}
	
	public function addAction() {
		if (Request::isPost ()) {
			$url = htmlspecialchars (Request::param ('url', ''));
			$lib = htmlspecialchars (Request::param ('libelle', ''));

			if (!empty ($url) && !empty ($lib)) {
				$lienDAO = new LienDAO();

				$values = array (
					'urlLien' => $url,
					'libelleLien' => $lib
				);

				if ($lienDAO->addLien ($values)) {
					$this->view->ok = 'Le lien a bien été ajouté';
				} else {
					$this->view->error = 'Un problème est survenu lors de l\'ajout du lien';
				}
			} else {
				$this->view->error = 'Tous les champs ne sont pas remplis correctement';
			}
		}

		$this->view->appendTitle('Ajouter un lien - ');
	}
	
	public function updateAction() {
		$this->view->appendTitle('Modifier un lien - ');
		
		$lienDAO = new LienDAO();
		$idLien = Request::param ('id');
		
		$lien = $lienDAO->searchById($idLien);
		
		if (!$lien) {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n\'existe pas'))
			);
		}
		
		if(Request::isPost ()) {
			$url = htmlspecialchars(Request::param ('url', ''));
			$lib = htmlspecialchars(Request::param ('libelle', ''));
			
			if(!empty($url) && !empty($lib)) {
				$lienDAO = new LienDAO();
				
				$values = array(
					'urlLien' => $url,
					'libelleLien' => $lib
				);
				
				if($lienDAO->updateLien($idLien, $values)) {
					$this->view->ok = 'L\'article a bien été modifié';
				
					$lien->_url ($url);
					$lien->_libelle ($lib);
				} else {
					$this->view->error = 'Un problème est survenu lors de la mise à jour de l\'article';
				}
			} else {
				$this->view->error = 'Tous les champs ne sont pas remplis correctement';
			}
		}
		
		$this->view->lien = $lienDAO->searchById($idLien);
	}
	
	public function deleteAction() {
		$idLien = Request::param ('id');

		if ($idLien !== false) {
			$lienDAO = new LienDAO();

			if ($lienDAO->deleteLien ($idLien)) {
				$this->view->ok = 'Le lien a bien été supprimé';
			} else {
				$this->view->error = 'Un problème est survenu lors de la suppression du lien';
			}
		} else {
			$this->view->error = 'Aucun identifiant n\'a été précisé';
		}
	}
}
