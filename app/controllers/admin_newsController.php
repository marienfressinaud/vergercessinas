<?php

class admin_newsController extends ActionController {
	public function firstAction () {
		$this->view->_layout('admin');
		View::appendStyle (Url::display ('/themes/default/admin.css'));
		View::appendStyle (Url::display ('/themes/default/simplemde.min.css'));
		View::appendStyle (Url::display ('/themes/default/font-awesome.min.css'));
		View::appendScript (Url::display ('/scripts/simplemde.min.js'));

		if (!is_admin ()) {
			MinzError::error (
				403,
				array ('warning' => array ('Vous n\'avez pas le droit d\'accéder à cette page'))
			);
		}
	}

	public function indexAction () {
		$articleDAO = new ArticleDAO ();

		if (Request::isPost ()) {
			$title = htmlspecialchars (Request::param ('title', ''));
			$content = Request::param ('content', '');
			$level = Request::param ('level');

			if (!empty ($title) && !empty ($content)) {
				$values = array (
					'titleArticle' => $title,
					'contentArticle' => $content,
					'dateArticle' => time (),
					'levelArticle' => $level
				);

				if ($articleDAO->addArticle ($values)) {
					$this->view->ok = 'L\'article a bien été publié';
				} else {
					$this->view->error = 'Un problème est survenu lors de la publication de l\'article';
				}
			} else {
				$this->view->error = 'Tous les champs ne sont pas remplis correctement';
			}
		}

		$articles = $articleDAO->lister ();

		//gestion pagination
		$page = Request::param ('page', 1);
		$this->view->articlesPaginator = new Paginator ($articles);
		$this->view->articlesPaginator->_nbItemsPerPage (25);
		$this->view->articlesPaginator->_currentPage ($page);

		View::prependTitle ('Gestion des news - ');
	}
	
	public function updateAction () {
		$articleDAO = new ArticleDAO ();
		$idArticle = Request::param ('id');

		$article = $articleDAO->searchById ($idArticle);
		
		if (!$article) {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n\'existe pas'))
			);
		}

		if (Request::isPost ()) {
			$title = htmlspecialchars (Request::param ('title', ''));
			$content = Request::param ('content', '');
			$level = Request::param ('level');

			if (!empty ($title) && !empty ($content)) {
				$values = array(
					'titleArticle' => $title,
					'contentArticle' => $content,
					'dateArticle' => $article->date (),
					'levelArticle' => $level
				);

				if ($articleDAO->updateArticle ($idArticle, $values)) {
					$this->view->ok = 'L\'article a bien été modifié';
				
					$article->_title ($title);
					$article->_content ($content);
					$article->_level ($level);
				} else {
					$this->view->error = 'Un problème est survenu lors de la mise à jour de l\'article';
				}
			} else {
				$this->view->error = 'Tous les champs ne sont pas remplis correctement';
			}
		}

		$this->view->article = $article;
		
		View::prependTitle ('Modifier une news - ');
	}

	public function deleteAction () {
		$idArticle = Request::param ('id');

		if ($idArticle !== false) {
			$articleDAO = new ArticleDAO ();

			if ($articleDAO->deleteArticle ($idArticle)) {
				$this->view->ok = 'L\'article a bien été supprimé';
			} else {
				$this->view->error = 'Un problème est survenu lors de la suppression de l\'article';
			}
		} else {
			$this->view->error = 'Aucun identifiant n\'a été précisé';
		}
	}
}
