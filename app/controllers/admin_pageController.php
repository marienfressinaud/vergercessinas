<?php

class admin_pageController extends ActionController {
	private $codeRetour = false;
	private $redirectAction = 'index';

	const PAGE_ADDED = 1;
	const PAGE_PROBLEM_ADD = -1;
	const PAGE_UPDATED = 2;
	const PAGE_PROBLEM_UPDATE = -2;
	const PAGE_DELETED = 3;
	const PAGE_PROBLEM_DELETE = -3;
	const NOT_HOME_PAGE = -4;
	const FORM_NOT_FILLED = -5;
	const LINE_ADDED = 6;
	const BLOCK_ADDED = 7;

	public function firstAction () {
		$this->view->_layout('admin');
		View::appendStyle (Url::display ('/themes/default/admin.css'));
		View::appendStyle (Url::display ('/themes/default/simplemde.min.css'));
		View::appendScript (Url::display ('/scripts/simplemde.min.js'));

		if (!is_admin ()) {
			MinzError::error (
				403,
				array ('warning' => array ('Vous n\'avez pas le droit d\'accéder à cette page'))
			);
		}
	}

	public function indexAction () {
		$pageDAO = new PageDAO ();
		$this->view->pages = $pageDAO->lister ();

		View::prependTitle('Gestion des pages - ');
	}

	public function addAction () {
		View::prependTitle ('Ajouter une page - ');

		if (Request::isPost ()) {
			$title = htmlspecialchars(Request::param ('title', ''));
			$alias = htmlspecialchars(Request::param ('alias', ''));
			if (empty ($alias)) {
				$alias = $title;
			}
			$location = Request::param ('location');

			if (empty ($title)) {
				$this->view->error = 'Tous les champs ne sont pas remplis correctement';
				return;
			}

			$pageDAO = new PageDAO ();
			$values = array (
				'titlePage'   => $title,
				'aliasPage'   => $alias,
				'datePage'    => time (),
				'locPage'     => $location,
				'layoutPage'  => '0',
			);

			$idPage = $pageDAO->addPage ($values);
			if ($idPage) {
				Request::forward(array(
					'c' => 'admin_page',
					'a' => 'update',
					'params' => array('id' => $idPage),
				));
			} else {
				$this->view->error = 'Un problème est survenu lors de l\'ajout de la page';
			}
		}
	}

	public function updateAction () {
		View::prependTitle ('Modifier une page - ');

		$pageDAO = new PageDAO ();
		$idPage = Request::param ('id');
		$page = $pageDAO->searchById ($idPage);

		if (!$page) {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n’existe pas'))
			);
			return;
		}

		$this->view->page = $page;

		if (Request::isPost ()) {
			$title = htmlspecialchars(Request::param ('title', ''));
			$alias = htmlspecialchars(Request::param ('alias', ''));
			if (empty ($alias)) {
				$alias = $title;
			}
			$location = Request::param ('location', Page::POS_ACCUEIL);

			if (empty ($title)) {
				$this->view->error = 'Tous les champs ne sont pas remplis correctement';
				return;
			}

			$values = array (
				'titlePage' => $title,
				'aliasPage' => $alias,
				'datePage' => time (),
				'locPage' => $location,
			);

			if ($pageDAO->updatePage ($idPage, $values)) {
				$this->view->ok = 'La page a bien été modifiée';
				$this->view->page = $pageDAO->searchById ($idPage);
			} else {
				$this->view->error = 'Un problème est survenu lors de la mise à jour de la page';
			}
		}
	}

	public function add_lineAction () {
		$pageDAO = new PageDAO ();
		$idPage = Request::param ('id');
		$page = $pageDAO->searchById ($idPage);

		if (!$page) {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n’existe pas'))
			);
			return;
		}

		$idBlock = $page->addLine();
		if ($idBlock) {
			Request::forward(array(
				'c' => 'admin_page_block',
				'a' => 'update',
				'params' => array('id' => $idBlock),
			), true);
		} else {
			Request::forward(array(
				'c' => 'admin_page',
				'a' => 'update',
				'params' => array('id' => $idPage),
			), true);
		}
	}

	public function add_blockAction () {
		$pageDAO = new PageDAO ();
		$idPage = Request::param ('id');
		$page = $pageDAO->searchById ($idPage);

		if (!$page) {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n\'existe pas'))
			);
			return;
		}

		$line = (int) Request::param ('line');
		$idBlock = $page->addBlockToLine($line);
		if ($idBlock) {
			Request::forward(array(
				'c' => 'admin_page_block',
				'a' => 'update',
				'params' => array('id' => $idBlock),
			), true);
		} else {
			Request::forward(array(
				'c' => 'admin_page',
				'a' => 'update',
				'params' => array('id' => $idPage),
			), true);
		}
	}

	public function deleteAction () {
		$idPage = Request::param ('id');

		if ($idPage !== false) {
			$pageDAO = new PageDAO ();

			$page = $pageDAO->searchById ($idPage);
			
			if (!$page) {
				MinzError::error (
					404,
					array ('error' => array ('La page que vous cherchez n\'existe pas'))
				);
			} elseif ($page->location () != Page::POS_ACCUEIL) {
				if ($pageDAO->deletePage ($idPage)) {
					$this->codeRetour = self::PAGE_DELETED;
				} else {
					$this->codeRetour = self::PAGE_PROBLEM_DELETE;
				}
			} else {
				$this->codeRetour = self::NOT_HOME_PAGE;
			}
		} else {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n\'existe pas'))
			);
		}
	}

	public function lastAction () {
		if ($this->codeRetour != false) {
			if($this->redirectAction != 'index') {
				$params = Request::params ();
			}
			$params['retour'] = $this->codeRetour;
			
			$url = array(
				'c' => 'admin_page',
				'a' => $this->redirectAction,
				'params' => $params
			);

			Request::forward ($url, true);
		}
		
		$this->checkMessageRetour ();
	}

	private function checkMessageRetour () {
		// gestion des messages de retour
		$code = Request::param ('retour');
		switch($code) {
		case self::PAGE_ADDED :
			$this->view->ok = 'La page a bien été publiée';
			break;
		case self::PAGE_PROBLEM_ADD :
			$this->view->error = 'Un problème est survenu lors de l\'ajout de la page';
			break;
		case self::PAGE_UPDATED : 
			$this->view->ok = 'La page a bien été modifiée';
			break;
		case self::PAGE_PROBLEM_UPDATE :
			$this->view->error = 'Un problème est survenu lors de la mise à jour de la page';
			break;
		case self::PAGE_DELETED : 
			$this->view->ok = 'La page a bien été suprimée';
			break;
		case self::PAGE_PROBLEM_DELETE :
			$this->view->error = 'Un problème est survenu lors de la suppression de la page';
			break;
		case self::NOT_HOME_PAGE :
			$this->view->error = 'Vous ne pouvez pas supprimer la page d\'accueil';
			break;
		case self::FORM_NOT_FILLED :
			$this->view->error = 'Tous les champs ne sont pas remplis correctement';
			break;
		default : 
		}
	}
}
