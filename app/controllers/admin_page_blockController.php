<?php

class admin_page_blockController extends ActionController {
	public function firstAction () {
		$this->view->_layout('admin');
		View::appendStyle (Url::display ('/themes/default/admin.css'));
		View::appendStyle (Url::display ('/themes/default/simplemde.min.css'));
		View::appendScript (Url::display ('/scripts/simplemde.min.js'));

		if (!is_admin ()) {
			MinzError::error (
				403,
				array ('warning' => array ('Vous n’avez pas le droit d’accéder à cette page'))
			);
		}
	}

	public function updateAction () {
		View::prependTitle ('Modifier un bloc - ');

		$pageBlockDAO = new PageBlockDAO ();
		$id = Request::param ('id');
		$pageBlock = $pageBlockDAO->searchById ($id);

		if (!$pageBlock) {
			MinzError::error (
				404,
				array ('error' => array ('Le bloc que vous cherchez n’existe pas'))
			);
			return;
		}

		$this->view->pageBlock = $pageBlock;

		if (Request::isPost ()) {
			$content = Request::param ('content');

			$values = array (
				'contentPageBlock' => $content,
			);

			if ($pageBlockDAO->update ($id, $values)) {
				Request::forward(array(
					'c' => 'admin_page',
					'a' => 'update',
					'params' => array('id' => $pageBlock->idPage()),
				), true);
			} else {
				$this->view->error = 'Une erreur est survenue lors de la modification du bloc.';
			}
		}
	}

	public function deleteAction () {
		View::prependTitle ('Supprimer un bloc - ');

		$pageBlockDAO = new PageBlockDAO ();
		$id = Request::param ('id');
		$pageBlock = $pageBlockDAO->searchById ($id);

		if (!$pageBlock) {
			MinzError::error (
				404,
				array ('error' => array ('Le bloc que vous cherchez n’existe pas'))
			);
			return;
		}

		$pageDAO = new PageDAO ();
		$page = $pageDAO->searchById ($pageBlock->idPage());

		if (!$page) {
			MinzError::error (
				404,
				array ('error' => array ('Le bloc que vous cherchez n’existe pas'))
			);
			return;
		}

		$page->deleteBlock($pageBlock);
		Request::forward(array(
			'c' => 'admin_page',
			'a' => 'update',
			'params' => array('id' => $pageBlock->idPage()),
		), true);
	}
}
