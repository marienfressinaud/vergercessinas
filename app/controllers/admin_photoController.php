<?php

class admin_photoController extends ActionController {
	public function firstAction () {
		$this->view->_layout('admin');
		View::appendStyle (Url::display ('/themes/default/admin.css'));

		if (!is_admin ()) {
			MinzError::error (
				403,
				array ('warning' => array ('Vous n\'avez pas le droit d\'accéder à cette page'))
			);
		}
	}

	public function indexAction () {
		View::prependTitle ('Gestion des photos - ');

		$photoDAO = new PhotoDAO ();
		$galerieDAO = new GalerieDAO ();

		$idGalerie = Request::param ('id');
		$galerie = $galerieDAO->searchById($idGalerie);

		if ($galerie !== false) {
			$this->view->galerie = $galerie;
		} else {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n\'existe pas'))
			);
		}

		$this->view->photos = $photoDAO->listerByGalerie ($idGalerie);
	}

	public function updateAction () {
		View::prependTitle ('Modifier une photo - ');

		$photoDAO = new PhotoDAO ();

		$idPhoto = Request::param ('id');

		if ($idPhoto !== false) {
			if (Request::isPost ()) {
				$photo = $photoDAO->searchById ($idPhoto);
				$ordre = $photo->ordre ();

				$titre = htmlspecialchars (Request::param ('titre', ''));
				$description = htmlspecialchars (Request::param ('description', ''));

				if (!empty ($titre)) {
					$values = array (
						'titrePhoto' => $titre,
						'descriptionPhoto' => $description,
						'ordrePhoto' => $ordre,
						'idGalerie' => $photo->idGalerie ()
					);

					if ($photoDAO->updatePhoto ($idPhoto, $values)) {
						$this->view->ok = 'La photo a bien été modifiée';
					} else {
						$this->view->error = 'Un problème est survenu lors de la mise à jour de la photo';
					}
				} else {
					$this->view->error = 'Tous les champs ne sont pas remplis correctement';
				}
			}

			$photo = $photoDAO->searchById ($idPhoto);
			if ($photo !== false) {
				$this->view->photo = $photo;
			} else {
				MinzError::error (
					404,
					array ('error' => array ('La page que vous cherchez n\'existe pas'))
				);
			}
		} else {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n\'existe pas'))
			);
		}
	}

	public function deleteAction () {
		View::prependTitle ('Supprimer une photo - ');

		$idPhoto = intval (Request::param ('id'));
		$photoDAO = new PhotoDAO ();
		$photo = $photoDAO->searchById ($idPhoto);

		if ($photo) {
			if ($photoDAO->deletePhoto ($idPhoto) && $photo->deleteFile ()) {
				$this->view->photo = $photo;
				$this->view->ok = 'La photo a été supprimée.';
			} else {
				$this->view->error = 'La photo n’a pas pu être supprimée correctement.';
			}
		} else {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n’existe pas'))
			);
		}
	}
}
