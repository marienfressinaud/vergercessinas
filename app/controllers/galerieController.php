<?php

class galerieController extends ActionController {
	public function indexAction () {
		$galerieDAO = new GalerieDAO ();
		$this->view->galeries = $galerieDAO->lister ();

		View::appendStyle (Url::display ('/themes/default/base.css'));
		View::prependTitle ('Diaporama - ');
	}

	public function seeAction () {
		$this->view->_layout ('galerie');
		View::appendScript (Url::display ('/scripts/galerie.js'));
		View::appendStyle (Url::display ('/themes/default/galerie.css'));

		$idGalerie = Request::param ('gal');
		$idPhoto = Request::param ('photo');

		$photoDAO = new PhotoDAO ();
		$galerieDAO = new GalerieDAO ();

		// récupère la galerie
		$galerie = $galerieDAO->searchById ($idGalerie);
		if (!$galerie) {
			MinzError::error (
				404,
				array ('error' => array ('La page que vous cherchez n\'existe pas'))
			);
			return;
		}

		// récupère la liste des photos
		$photos = $photoDAO->listerByGalerie ($idGalerie);

		// récupère la photo
		$photo = $photoDAO->searchById ($idPhoto);
		if (!$photo) {
			$photo = $photos[0] ?? null;
		}

		// Calcule les photos précédentes et suivantes
		if (count($photos) > 1) {
			$current_index = array_search($photo, $photos);

			if ($current_index - 1 >= 0) {
				$this->view->previousPhoto = $photos[$current_index - 1];
			}
			if ($current_index + 1 < count($photos)) {
				$this->view->nextPhoto = $photos[$current_index + 1];
			}
		}

		$this->view->galerie = $galerie;
		$this->view->photos = $photos;
		$this->view->photo = $photo;

		View::prependTitle ($galerie->nom () . ' - ');
	}
}
