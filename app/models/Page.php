<?php

class Page extends Model {
	private $id;
	private $title;
	private $alias;
	private $date;
	private $location;
	private $layout;

	const POS_ACCUEIL = 0;
	const POS_HEADER = 1;
	const POS_MENU = 2;
	const POS_FOOTER = 3;

	public function id () { return $this->id; }
	public function title () { return $this->title; }
	public function alias () { return $this->alias; }
	public function date () { return $this->date; }
	public function location () { return $this->location; }
	public function layout () {
		foreach (str_split ($this->layout) as $nb_blocks) {
			yield (int) $nb_blocks;
		}
	}
	public function rawLayout () {
		return $this->layout;
	}

	public function blocks () {
		$pageBlockPDO = new PageBlockDAO ();
		$blocks = $pageBlockPDO->listByPage ($this->id);
		$current_block = 0;

		foreach ($this->layout () as $nb_blocks) {
			$next_block = $current_block + $nb_blocks;
			yield array_slice($blocks, $current_block, $nb_blocks);
			$current_block = $next_block;
		}
	}
	public function nbBlocks() {
		$pageBlockPDO = new PageBlockDAO ();
		return count($pageBlockPDO->listByPage ($this->id));
	}

	public function _id ($id) { $this->id = $id; }
	public function _title ($title) { $this->title = $title; }
	public function _alias ($alias) { $this->alias = $alias; }
	public function _date ($date) { $this->date = $date; }
	public function _location ($location) { $this->location = $location; }
	public function _layout ($layout) { $this->layout = $layout; }

	public function addLine () {
		$new_layout = $this->layout . '0';
		$dao = new PageDAO ();
		if (!$dao->updateLayoutPage ($this->id, $new_layout)) {
			return false;
		}
		$this->layout = $new_layout;
		$line = strlen($new_layout) - 1;
		return $this->addBlockToLine($line);
	}

	public function addBlockToLine ($line) {
		$new_layout = '';
		$increment_order = true;
		$order = 1;
		foreach ($this->layout () as $line_number => $nb_blocks) {
			if ($increment_order) {
				$order += $nb_blocks;
			}
			if ($line === $line_number) {
				$nb_blocks += 1;
				$increment_order = false;
			}
			$new_layout .= $nb_blocks;
		}

		$pageDao = new PageDAO ();
		if (!$pageDao->updateLayoutPage ($this->id, $new_layout)) {
			return false;
		}

		$pageBlockDao = new PageBlockDAO ();
		return $pageBlockDao->insertBlockAt($this->id, $order);
	}

	public function deleteBlock ($block) {
		$order = $block->order();
		$block_found = false;
		$total_blocks = 0;
		$new_layout = '';
		foreach ($this->layout () as $line_number => $nb_blocks) {
			$total_blocks += $nb_blocks;
			if (!$block_found && $total_blocks >= $order) {
				$nb_blocks -= 1;
				$block_found = true;
			}
			if ($nb_blocks > 0) {
				$new_layout .= $nb_blocks;
			}
		}
		if (empty($new_layout)) {
			$new_layout = '0';
		}

		$pageDao = new PageDAO ();
		if (!$pageDao->updateLayoutPage ($this->id, $new_layout)) {
			return false;
		}

		$pageBlockDao = new PageBlockDAO ();
		return $pageBlockDao->delete($block);
	}
}

class PageDAO extends Model_pdo {
	public function __construct() {
		parent::__construct();
	}

	public function lister() {
		$sql = 'SELECT * FROM page';
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ();

		return HelperPage::listeDaoToPage ($stm->fetchAll (PDO::FETCH_CLASS));
	}

	public function listerByLocation ($location) {
		$sql = 'SELECT * FROM page WHERE locPage=?';
		$values = array ($location);
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ($values);

		return HelperPage::listeDaoToPage ($stm->fetchAll (PDO::FETCH_CLASS));
	}

	public function searchById ($id) {
		$sql = 'SELECT * FROM page WHERE idPage=?';
		$values = array ($id);
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ($values);
		$res = $stm->fetchAll (PDO::FETCH_CLASS);

		if (!empty ($res)) {
			return HelperPage::daoToPage ($res[0]);
		} else {
			return false;
		}
	}

	public function searchMainPage () {
		$sql = 'SELECT * FROM page WHERE locPage=0';
		$stm = $this->bd->prepare ($sql); 
		$stm->execute ();
		$res = $stm->fetchAll (PDO::FETCH_CLASS);
		
		if (!empty ($res)) {
			return HelperPage::daoToPage ($res[0]);
		} else {
			return false;
		}
	}

	public function addPage ($valuesTmp) {
		$sql = 'INSERT INTO page(contentPage, titlePage, aliasPage, datePage, locPage) VALUES("",?,?,?,?)';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['titlePage'],
			$valuesTmp['aliasPage'],
			$valuesTmp['datePage'],
			$valuesTmp['locPage']
		);

		if ($stm && $stm->execute ($values)) {
			return $this->bd->lastInsertId();
		} else {
			return false;
		}
	}

	public function addDefaultPage () {
		$values = array(
			'titlePage' => 'Bienvenue',
			'aliasPage' => 'Accueil',
			'datePage' => time(),
			'locPage' => Page::POS_ACCUEIL
		);
		$this->addPage($values);
	}

	public function updatePage ($id, $valuesTmp) {
		$sql = 'UPDATE page SET titlePage=?, aliasPage=?, datePage=?, locPage=? WHERE idPage=?';
		$stm = $this->bd->prepare ($sql);

		$values = array (
			$valuesTmp['titlePage'],
			$valuesTmp['aliasPage'],
			$valuesTmp['datePage'],
			$valuesTmp['locPage'],
			$id
		);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateLayoutPage ($id, $layout) {
		$sql = 'UPDATE page SET layoutPage=? WHERE idPage=?';
		$stm = $this->bd->prepare ($sql);

		$values = array ($layout, $id);
		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}

	public function deletePage ($id) {
		$sql = 'DELETE FROM page WHERE idPage=?';
		$stm = $this->bd->prepare ($sql); 

		$values = array ($id);

		if ($stm && $stm->execute ($values)) {
			return true;
		} else {
			return false;
		}
	}
}

class HelperPage {
	public static function daoToPage ($dao) {
		if (!is_array ($dao)) {
			$dao = array ($dao);
		}
		
		$pages = HelperPage::listeDaoToPage ($dao);

		return $pages[0];
	}

	public static function listeDaoToPage ($listeDAO) {
		$liste = array ();

		foreach ($listeDAO as $key => $dao) {
			$liste[$key] = new Page ();
			$liste[$key]->_id ($dao->idPage);
			$liste[$key]->_title ($dao->titlePage);
			$liste[$key]->_alias ($dao->aliasPage);
			$liste[$key]->_date ($dao->datePage);
			$liste[$key]->_location ($dao->locPage);
			$liste[$key]->_layout ($dao->layoutPage);
		}

		return $liste;
	}
}
