<?php

function filterChar ($in) {
	$char = array (
		'@(é|è|ê|ë|Ê|Ë|€|É|È)@i'   => 'e',
		'@(á|ã|à|â|ä|À|Â|Ä|Ã)@i'   => 'a',
		'@(ì|í|î|ï|Î|Ï)@i'         => 'i',
		'@(ú|û|ù|ü|Û|Ü)@i'         => 'u',
		'@(ò|ó|õ|ô|ö|Ô|Ö)@i'       => 'o',
		'@(ñ|Ñ)@i'                 => 'n',
		'@(ý|ÿ|Ý)@i'               => 'y',
		'@(ç)@i'                   => 'c',
		'@(æ)@i'                   => 'ae',
		'@( )@i'                   => '-',
		'@(\w+)(-){2,}@'           => '$1-', // supprime les tirets qui se suivent
		'@[^a-zA-Z0-9_\-]@'        => '',
	);
	
	$in = preg_replace (array_flip ($char), $char, $in);
	
	return mb_strtolower ($in);
}
