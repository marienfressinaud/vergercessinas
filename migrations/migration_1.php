<?php

class Migration1 {
	public function up($db) {
		$sql = 'ALTER TABLE page ADD COLUMN layoutPage varchar(50) NOT NULL DEFAULT "0"';
		$statement = $db->prepare($sql);
		$res = $statement->execute();
		if (!$res) {
			return false;
		}

		$sql = <<<SQL
CREATE TABLE `pageBlock` (
  `idPageBlock` integer NOT NULL PRIMARY KEY AUTOINCREMENT
,  `contentPageBlock` text NOT NULL
,  `orderPageBlock` integer NOT NULL
,  `idPage` integer NOT NULL
,  CONSTRAINT `pageBlock_ibfk_1` FOREIGN KEY (`idPage`) REFERENCES `page` (`idPage`)
)
SQL;
		$statement = $db->prepare($sql);
		$res = $statement->execute();
		if (!$res) {
			return false;
		}

		$sql = 'SELECT idPage, contentPage FROM page';
		$statement = $db->prepare($sql);
		$res = $statement->execute();
		if (!$res) {
			return false;
		}

		$pages = $statement->fetchAll(PDO::FETCH_CLASS);
		foreach ($pages as $page) {
			$sql = 'INSERT INTO pageBlock(contentPageBlock, orderPageBlock, idPage) VALUES(?, ?, ?)';
			$statement = $db->prepare($sql);
			$res = $statement->execute(array(
				$page->contentPage,
				1,
				$page->idPage,
			));

			if (!$res) {
				return false;
			}
		}

		$sql = 'UPDATE page SET layoutPage="1"';
		$stm = $db->prepare ($sql);
		return $stm->execute ();
	}
}
