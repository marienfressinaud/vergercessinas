function handleGalerieShortcuts (e) {
    var element;
    if (e.key === 'Backspace') {
        element = document.getElementById('to-galleries');
    }

    if (e.key === 'o') {
        element = document.getElementById('to-original');
    }

    if (e.key === 'ArrowDown' || e.key === 'j') {
        element = document.getElementById('to-gallery');
    }

    if (e.key === 'ArrowUp' || e.key === 'k') {
        element = document.getElementById('to-up');
    }

    if (e.key === 'ArrowLeft' || e.key === 'h') {
        element = document.getElementById('to-previous');
    }

    if (e.key === 'ArrowRight' || e.key === 'l') {
        element = document.getElementById('to-next');
    }

    if (element != null) {
        element.click();
    }
}

document.addEventListener('keyup', handleGalerieShortcuts, false);
